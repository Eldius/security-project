package net.eldiosantos.gradle.persistence.model;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Created by esjunior on 15/07/2016.
 */
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserCredentials {

    @Id
    @GeneratedValue
    private Long id;
    private String login;
    private String pass;
    private String salt;

    @Column(nullable = false)
    private Boolean active = Boolean.FALSE;
}
