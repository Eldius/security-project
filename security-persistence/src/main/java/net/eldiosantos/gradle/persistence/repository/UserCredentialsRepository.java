package net.eldiosantos.gradle.persistence.repository;

import net.eldiosantos.gradle.persistence.model.UserCredentials;
import org.springframework.data.repository.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Created by Eldius on 16/07/2016.
 */
public interface UserCredentialsRepository extends Repository<UserCredentials, Long> {
    Optional<UserCredentials> findOne(Long id);
    List<UserCredentials> findAll();
}
