package net.eldiosantos.gradle.persistence.util;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Created by esjunior on 15/07/2016.
 */
@ApplicationScoped
public class HibernateEntityManagerCreator {

    private final EntityManagerFactory factory;

    public HibernateEntityManagerCreator() {
        factory = Persistence.createEntityManagerFactory( "users" );
    }

    @Produces
    public EntityManager getInstance() {
        return factory.createEntityManager();
    }
}
