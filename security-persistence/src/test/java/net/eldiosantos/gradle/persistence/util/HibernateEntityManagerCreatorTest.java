package net.eldiosantos.gradle.persistence.util;

import net.eldiosantos.gradle.persistence.model.UserCredentials;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;

import static com.google.common.truth.Truth.assertThat;
import static com.google.common.truth.Truth.assertWithMessage;

/**
 * Created by esjunior on 15/07/2016.
 */
public class HibernateEntityManagerCreatorTest {

    private HibernateEntityManagerCreator creator = new HibernateEntityManagerCreator();

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void getInstance() throws Exception {
        final EntityManager em = creator.getInstance();

        assertWithMessage("We shouldn't have a null Entity Manager").that(em).isNotNull();
    }

    @Test
    public void testInsert() throws Exception {
        final EntityManager em = creator.getInstance();

        EntityTransaction tx = em.getTransaction();
        em.persist(
            UserCredentials
                .builder()
                .login("eldius")
                .pass("MyStrongPass")
                .salt("salt")
                .active(Boolean.TRUE)
                .build()
        );
        tx.commit();

        TypedQuery<UserCredentials> query = em.createNamedQuery("listUsers", UserCredentials.class);

        assertWithMessage("Added a new user?").that(query.getResultList()).isNotEmpty();
    }

}
